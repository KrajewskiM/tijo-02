package com.company;
import java.io.BufferedReader;
        import java.io.FileNotFoundException;
        import java.io.FileReader;
        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.List;

interface CsvReport {
    /**
     * funkcja zwraca liste stringow - lista linii z pliku CSV
     */

    List<String> csvToListOfString();
}

interface LineConverter {
    /**
     * wykonuje konwersje linii zapisanych w odpowiednim
     * formacie do listy obiektow typu Team
     * funkcja przyjmuje liste linii z pliku CSV oraz separator rekordow
     */
    List<Team> toTeam(List<String> lines, String splitBy);
}

class Team {
    private int id;
    private String name;
    private int points;
    private int wins;
    private int loses;
    private int draws;

    public Team(int id, String name, int points, int wins, int loses, int draws) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", points=" + points +
                ", wins=" + wins +
                ", loses=" + loses +
                ", draws=" + draws +
                '}';
    }
    // prosze zaprojektowac klase Team
// prosze zadbac o odpowiednia implementacje pol klasy
// prosze zaimplementowac odpowiedni konstruktor oraz metody,
// ktore zwracaja pola klasy
// prosze zaimplementowac metode toString
// przed implementacja prosze przeanalizowac plik CSV
// (http://tomaszgadek.com/static/ekstraklasa.csv)
// oraz klase Main
}

class LineConverterImpl implements LineConverter {
    @Override
            /*
             team.getId(), team.getName(), team.getPoints(), team.getWins(), team.getLoses(),
                    team.getDraws());
             */

    public List<Team> toTeam(List<String> lines, String splitBy) {
        List<Team> teams = new ArrayList<>();

        for (String line : lines) {
            String[] lineSplit = line.split(splitBy);
            int id = Integer.parseInt(lineSplit[0].trim());
            String name = lineSplit[1].trim();
            int point = Integer.parseInt(lineSplit[2].trim());
            int wins = Integer.parseInt(lineSplit[3].trim());
            int loses = Integer.parseInt(lineSplit[4].trim());
            int draws = Integer.parseInt(lineSplit[5].trim());

            teams.add(new Team(id, name, point, wins, loses, draws));

        }
        return teams;
    }
// klasa LineConverterImpl powinna implementowac interfejs LineConverter
// prosze zaimplementowac metode toTeam
// przed implementacja prosze przeanalizowac klase Main
}

class CsvReader implements CsvReport {
    private final String csvFile;

    public CsvReader(String csvFile) {
        this.csvFile = csvFile;
    }

    @Override
    public List<String> csvToListOfString() {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        List<String> list = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] teams = line.split(cvsSplitBy);
                list.add(line);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }

}


// klasa CsvReader powinna implementowac interfejs CsvReport
// prosze utworzyc odpowiedni konstruktor
// oraz wlasna implementacje metody csvToListOfString
// przed implementacja prosze przeanalizowac klase Main


// bardzo prosze nie modyfikowac klasy Main
class Main {
    public static void main(String[] args) {

        final String CSV_FILE = "C:\\Users\\student\\Documents\\k\\Lab2\\src\\ekstraklasa.csv";
        final String SPLIT_BY = ";";
        CsvReport csvReport = new CsvReader(CSV_FILE);
        LineConverter lineConverter = new LineConverterImpl();
        List<String> lines = csvReport.csvToListOfString();
        List<Team> teams = lineConverter.toTeam(lines, SPLIT_BY);
        for (Team team : teams) {
            System.out.printf("%2d: %-25s: Punkty: %2d, Zwyciestwa: %2d, Porazki: %2d, Remisy: %2d \n",
                    team.getId(), team.getName(), team.getPoints(), team.getWins(), team.getLoses(),
                    team.getDraws());
        }

    }

}